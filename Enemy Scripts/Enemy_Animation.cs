﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Enemy_Animation : MonoBehaviour {

        private Enemy_Master enemyMaster;
        private Animator myAnimator;

        void OnEnable() {
            SetInitialReferences();
            enemyMaster.EventEnemyDie += DisableAnimator;
            enemyMaster.EventEnemyWalking += SetAnimationToWalk;
            enemyMaster.EventEnemyReachedNavTarget += SetAnimationToIdle;
            enemyMaster.EventEnemyAttack += SetAnimationToAttack;
            enemyMaster.EventEnemyDeductHealth += SetAnimationToStruck;
        }

        void OnDisable(){
            enemyMaster.EventEnemyDie -= DisableAnimator;
            enemyMaster.EventEnemyWalking -= SetAnimationToWalk;
            enemyMaster.EventEnemyReachedNavTarget -= SetAnimationToIdle;
            enemyMaster.EventEnemyAttack -= SetAnimationToAttack;
            enemyMaster.EventEnemyDeductHealth -= SetAnimationToStruck;
        }
	
		void SetInitialReferences(){
            enemyMaster = GetComponent<Enemy_Master>();
            if(GetComponent<Animator>() != null)
                myAnimator = GetComponent<Animator>();
		}

        void SetAnimationToWalk() {
            if (myAnimator != null && myAnimator.enabled)
                myAnimator.SetBool("isPursuing", true);
        }

        void SetAnimationToIdle() {
            if (myAnimator != null && myAnimator.enabled)
                myAnimator.SetBool("isPursuing", false);
        }

        void SetAnimationToAttack() {
            if (myAnimator != null && myAnimator.enabled)
                myAnimator.SetTrigger("Attack");
        }

        void SetAnimationToStruck(float dummy) {
            if (myAnimator != null && myAnimator.enabled)
                myAnimator.SetTrigger("Faint");
        }

        void DisableAnimator() {
            if (myAnimator != null)
                myAnimator.enabled = false;
        }
    }
}