﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Enemy_DropItem : MonoBehaviour {

        private Enemy_Master enemyMaster;

		void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += DropItem;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDie -= DropItem;
        }

		void SetInitialReferences(){
            enemyMaster = GetComponentInParent<Enemy_Master>();
		}

        void DropItem() {
            transform.SetParent(null);
            GetComponent<Rigidbody>().isKinematic = false;
            Destroy(this);
        }
	}
}