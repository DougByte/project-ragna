﻿using UnityEngine;
using System.Collections.Generic;
using MEC;

namespace Ragna {

	public class Enemy_Health : MonoBehaviour {
        private Enemy_Master enemyMaster;
        public float enemyHealth;

        void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDeductHealth += DeductHealth;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDeductHealth -= DeductHealth;
        }

		void SetInitialReferences(){
            enemyMaster = GetComponent<Enemy_Master>();
		}

        void DeductHealth(float healthChange) {

            enemyHealth -= healthChange;
            if(enemyHealth <= 0) {
                enemyHealth = 0;
                enemyMaster.CallEventEnemyDie();
                Timing.RunCoroutine(_DisableEnemy(), enemyMaster.self);
            }
        }

        IEnumerator<float> _DisableEnemy() {
            yield return Timing.WaitForSeconds(Random.Range(10, 21));
            enemyHealth = 100;
            gameObject.SetActive(false);
        }
	}
}