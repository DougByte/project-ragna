﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using MEC;

namespace Ragna {

    [RequireComponent(typeof(NavMeshAgent))]
    public class Enemy_NavPause : MonoBehaviour {

        private Enemy_Master enemyMaster;
        private NavMeshAgent myNavMeshAgent;

        void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += DisaleThis;
            enemyMaster.EventEnemyFreeze += PauseNavMeshAgent;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDie -= DisaleThis;
            enemyMaster.EventEnemyFreeze -= PauseNavMeshAgent;
        }
	
		void SetInitialReferences(){
            enemyMaster = GetComponentInParent<Enemy_Master>();
            myNavMeshAgent = GetComponentInParent<NavMeshAgent>();
        }

        public void PauseNavMeshAgent(float pauseTime = 1) {
            if (myNavMeshAgent.enabled) {
                myNavMeshAgent.ResetPath();
                enemyMaster.isNavPaused = true;
                Timing.RunCoroutine(_RestartNavMeshAgent(pauseTime), enemyMaster.self);
            }
        }

        IEnumerator<float> _RestartNavMeshAgent(float pauseTime) {
            yield return Timing.WaitForSeconds(pauseTime);
            enemyMaster.isNavPaused = false;    
        }

        void DisaleThis() {
            Timing.KillCoroutines(enemyMaster.self);
        }
	}
}