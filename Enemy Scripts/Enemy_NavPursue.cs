﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
	public class Enemy_NavPursue : MonoBehaviour {

        private Enemy_Master enemyMaster;
        private UnityEngine.AI.NavMeshAgent myNavMeshAgent;
        private float checkRate;
        private float nextCheck;

		void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDie -= DisableThis;
        }
	
		void Update () {
	        if(Time.time > nextCheck && myNavMeshAgent.enabled) {
                nextCheck = Time.time + checkRate;
                TryToChaseTarget();
                CheckIfDestinationReached();
            }
		}

		void SetInitialReferences(){
            enemyMaster = GetComponent<Enemy_Master>();
            myNavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            checkRate = Random.Range(0.1f, 0.2f);
		}

        void TryToChaseTarget() {
            if(enemyMaster.target != null && !enemyMaster.isNavPaused) {
                myNavMeshAgent.SetDestination(enemyMaster.target.position);

                if (myNavMeshAgent.remainingDistance > myNavMeshAgent.stoppingDistance) {
                    enemyMaster.CallEventEnemyWalking();
                    enemyMaster.isOnRoute = true;
                }
            }
        }

        void CheckIfDestinationReached() {
            if (enemyMaster.isOnRoute && myNavMeshAgent.remainingDistance < myNavMeshAgent.stoppingDistance) {
                enemyMaster.isOnRoute = false;
                enemyMaster.CallEventEnemyReachedNavTarget();
            }
        }

        void DisableThis() {
            myNavMeshAgent.enabled = false;
            enabled = false;
        }
	}
}