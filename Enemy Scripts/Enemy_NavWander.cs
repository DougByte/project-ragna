﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class Enemy_NavWander : MonoBehaviour {

        private Enemy_Master enemyMaster;
        private UnityEngine.AI.NavMeshAgent myNavMeshAgent;
        private float checkRate;
        private float nextCheck;
        private float wanderRange = 10;
        private Transform myTransform;
        private UnityEngine.AI.NavMeshHit navHit;
        private Vector3 wanderTarget;

        void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += DisableThis;
        }

		void OnDisable(){
            enemyMaster.EventEnemyDie -= DisableThis;
        }
	
		void Update () {
            if (Time.time > nextCheck) {
                nextCheck = Time.time + checkRate;
                CheckIfShouldWander();
            }
        }

		void SetInitialReferences(){
            enemyMaster = GetComponent<Enemy_Master>();
            myNavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            checkRate = Random.Range(0.1f, 0.2f);
            myTransform = transform;
        }

        void CheckIfShouldWander() {
            if (enemyMaster.target == null && !enemyMaster.isOnRoute && !enemyMaster.isNavPaused)
                if (RandomWanderTarget(myTransform.position, wanderRange, out wanderTarget)) {
                    myNavMeshAgent.SetDestination(wanderTarget);
                    enemyMaster.isOnRoute = true;
                    enemyMaster.CallEventEnemyWalking();
                }
        }

        bool RandomWanderTarget(Vector3 center, float range, out Vector3 result) {
            Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;

            if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, UnityEngine.AI.NavMesh.AllAreas)) {
                result = navHit.position;
                return true;
            } else {
                result = center;
                return false;
            }
        }

        void DisableThis() {
            enabled = false;
        }
    }
}