﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class Enemy_RagdollActivation : MonoBehaviour {

        private Enemy_Master enemyMaster;
        private Rigidbody myRigidBody;
        private Collider myCollider;

		void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += ActivateRagdoll;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDie -= ActivateRagdoll;
        }

		void SetInitialReferences(){
            enemyMaster = transform.GetComponentInParent<Enemy_Master>();
            myRigidBody = GetComponent<Rigidbody>();
            myCollider = GetComponent<Collider>();
		}

        void ActivateRagdoll() {
            myRigidBody.isKinematic = false;
            myRigidBody.useGravity = true;
            myCollider.enabled = true;
            myCollider.isTrigger = false;
        }
	}
}