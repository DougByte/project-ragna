﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Enemy_SpawnGroup : MonoBehaviour {

        public GameObject objectToSpawn;
        public int numberToSpawn;
        public float proximity;
        private float checkRate;
        private float nextCheck;
        private Transform myTransform;
        private Transform[] playerTransform;
        private Vector3 spawnPosition;
        
		void Start () {
            SetInitialReferences();
		}
	
		void Update () {
            CheckDistance();
		}

        void OnDisable() {
            GameManager_References.instance.gameManagerMaster.AddControllerEvent -= UpdatePlayersTransform;
        }

		void SetInitialReferences(){
            myTransform = transform;
            UpdatePlayersTransform();
            checkRate = Random.Range(0.8f, 1.2f);
            GameManager_References.instance.gameManagerMaster.AddControllerEvent += UpdatePlayersTransform;
        }

        void CheckDistance() {
            if(Time.time > nextCheck) {
                nextCheck = Time.time + checkRate;
                for (int i = 0; i < playerTransform.Length; i++)
                    if (Vector3.Distance(myTransform.position, playerTransform[i].position) < proximity) {
                        SpawnObjects();
                        enabled = false;
                    }
            }
        }

        void SpawnObjects() {
            for(int i = 0; i < numberToSpawn; i++) {
                spawnPosition = myTransform.position + Random.insideUnitSphere * numberToSpawn;
                spawnPosition.y = 120.01f;
                Instantiate(objectToSpawn, spawnPosition, myTransform.rotation);
            }
        }

        void UpdatePlayersTransform() {
            playerTransform = new Transform[GameManager_References.instance.players.Length];
            for (int i = 0; i < GameManager_References.instance.players.Length; i++)
                playerTransform[i] = GameManager_References.instance.players[i].transform;
        }
	}
}