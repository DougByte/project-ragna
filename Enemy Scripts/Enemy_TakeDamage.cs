﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Enemy_TakeDamage : MonoBehaviour {

        private Enemy_Master enemyMaster;
        public float damegeMultiplier = 1;
        public bool shouldRemoveCollider;

		void OnEnable(){
            SetInitialReferences();
            enemyMaster.EventEnemyDie += RemoveThis;
		}

		void OnDisable(){
            enemyMaster.EventEnemyDie -= RemoveThis;
        }

		void SetInitialReferences(){
            enemyMaster = transform.GetComponentInParent<Enemy_Master>();
		}

        public void ProcessDamage(float damage) {
            float damageToApply = damage * damegeMultiplier;
            enemyMaster.CallEventEnemyDeductHealth(damageToApply);
        }

        public void Freeze(float freezeTime){
            enemyMaster.CallEventEnemyFreeze(freezeTime);
        }

        void RemoveThis() {
            if (shouldRemoveCollider && GetComponent<Collider>() != null)
                gameObject.SetActive(false);
        }
	}
}