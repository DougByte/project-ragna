﻿using UnityEngine;

namespace Ragna {

    public class GameManager_ControllerManager : MonoBehaviour {

        private static int numberOfControllers = 0;
        public static bool[] isControllerConnected = { false, false, false, false };
        public static string[] controller = { "", "", "", "" };
        public static string start, pcHorizontal, pcVertical, pcXAxis, pcYAxis, pcZoom, pcRightHand, pcLeftHand, pcRun, pcJump, pcEnter, pcAction, pcInventory;
        public static string xboxHorizontal, xboxVertical, xboxXAxis, xboxYAxis, xboxTriggers, xboxA, xboxB, xboxX, xboxY, xboxBack, xboxLB, xboxRB, xboxLS, xboxRS, xboxDPadH, xboxDPadV;
        public static int NumberOfControlers { get { return numberOfControllers; } set { numberOfControllers = value; } }

        void OnEnable(){
            SetInitialReferences();
		}

		void OnDisable(){
            GameManager_References.instance.gameManagerMaster.AddControllerEvent -= DetectController;
            GameManager_References.instance.gameManagerMaster.AddControllerEvent -= Controls;
        }
	
		void SetInitialReferences(){
            SetDefaultValues();
            GameManager_References.instance.gameManagerMaster.AddControllerEvent += DetectController;
            GameManager_References.instance.gameManagerMaster.AddControllerEvent += Controls;
            GameManager_References.instance.gameManagerMaster.CallAddControllerEvent();
        }

        void DetectController() {
            try {
                for(int i = 0; i < 4; i++)
                    if (Input.GetJoystickNames()[i] != null) {
                        isControllerConnected[i] = true;
                        controller[i] = Input.GetJoystickNames()[i];
                        NumberOfControlers++;
                    }
            }
            catch {
                for (int i = 0; i < 4; i++)
                    if (controller[i] == "") {
                        isControllerConnected[i] = false;
                        NumberOfControlers--;
                        if (NumberOfControlers < 0)
                            NumberOfControlers = 0;
                    }
            }
        }

        void SetDefaultValues() {
            start = "Start";
            pcHorizontal = "P1_Horizontal";
            pcVertical = "P1_Vertical";
            pcXAxis = "Mouse_X";
            pcYAxis = "Mouse_Y";
            pcZoom = "Mouse_Scroll_Wheel";
            pcRightHand = "P1_RHand";
            pcLeftHand = "P1_LHand";
            pcRun = "P1_Run";
            pcJump = "P1_Jump";
            pcEnter = "P1_Enter";
            pcAction = "P1_Action";
            pcInventory = "P1_Inventory";

            xboxHorizontal = "Horizontal";
            xboxVertical = "Vertical";
            xboxXAxis = "AxisX";
            xboxYAxis = "AxisY";
            xboxTriggers = "Triggers";
            xboxA = "A";
            xboxB = "B";
            xboxX = "X";
            xboxY = "Y";
            xboxBack = "Back";
            xboxLB = "LB";
            xboxRB = "RB";
            xboxLS = "LS";
            xboxRS = "RS";
            xboxDPadH = "DPadH";
            xboxDPadV = "DPadV";
        }
        
        void Controls() {
            int playerCount = 0;
            for(int i = 0; i < 4; i++) {
                if (GameManager_References.instance.players[i].activeSelf)
                    playerCount++;
            }

            if (!isControllerConnected[playerCount - 1]) {
                GameManager_References.instance.players[0].GetComponent<Player_Master>().useXboxController = false;
                for (int i = 1; i < 4; i++)
                    GameManager_References.instance.players[i].GetComponent<Player_Master>().playerIndex = "J" + i + "_";
            } else {
                GameManager_References.instance.players[0].GetComponent<Player_Master>().useXboxController = true;
                for (int i = 1; i < 4; i++)
                    GameManager_References.instance.players[i].GetComponent<Player_Master>().playerIndex = "J" + (i + 1) + "_";
            }
        }
    }
}