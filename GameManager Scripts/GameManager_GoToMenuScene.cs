﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Ragna {

	public class GameManager_GoToMenuScene : MonoBehaviour {

        private GameManager_Master gameManagerMaster;

		void OnEnable(){
            SetInitialReferences();
            gameManagerMaster.GoToMenuSceneEvent += GotoMenuScene;
		}

		void OnDisable(){
            gameManagerMaster.GoToMenuSceneEvent -= GotoMenuScene;
        }

		void SetInitialReferences(){
            gameManagerMaster = GetComponent<GameManager_Master>();
		}

        void GotoMenuScene() {
            SceneManager.LoadScene(0);
        }
	}
}