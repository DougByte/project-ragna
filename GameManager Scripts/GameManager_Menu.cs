﻿using UnityEngine;

namespace Ragna {

    public class GameManager_Menu : MonoBehaviour {

        public void StartGame() {
            GameManager_SceneManager.SwitchScene("Game Scene");
        }

        public void Options() {

        }

        public void AddRemovePlayer(bool addOrRemove) {
            if (addOrRemove)
                for (int i = 1; i < 4; i++) {
                    if (!GameManager_References.instance.players[i].activeSelf) {
                        GameManager_References.instance.players[i].SetActive(addOrRemove);
                        break;
                    }
                } else if (GameManager_References.instance.players[1].activeSelf)
                for (int i = 3; i > 0; i--)
                    if (GameManager_References.instance.players[i].activeSelf) {
                        GameManager_References.instance.players[i].SetActive(addOrRemove);
                        break;
                    }
            GameManager_References.instance.gameManagerMaster.CallAddControllerEvent();
        }

        public void SaveGame() {

        }

        public void LoadGame() {

        }

        public void ExitGame() {
            Application.Quit();
        }

    }
}