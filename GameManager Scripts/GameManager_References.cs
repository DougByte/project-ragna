﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class GameManager_References : MonoBehaviour {

        public string playerTag;
        public string enemyTag;
        public LayerMask enemyLayer;
        public RectTransform inventory;
        public Transform grid;
        public Transform playerHead;
        public Transform playerChest;
        public Transform playerPants;
        public Transform playerRightFoot;
        public Transform playerLeftFoot;
        public Transform playerRightHand;
        public Transform playerLeftHand;
        public GameObject[] players;

        [HideInInspector]public bool[] isAttacking = new bool[4];
        [HideInInspector] public float[] currentAttackAnimationLength = new float[4];
        [HideInInspector] public Transform[,] slots = new Transform[5, 10];
        [HideInInspector] public GameManager_Master gameManagerMaster;

        public static GameManager_References instance;

        void OnEnable(){
            instance = this;
            gameManagerMaster = GetComponent<GameManager_Master>();
            CheckForWarnings();
            SetInitialReferences();           
        }

		void SetInitialReferences(){
            if(inventory == null)
                inventory = GameObject.Find("Inventory").GetComponent<RectTransform>();
            if(grid == null)
                grid = GameObject.Find("Grid").GetComponent<Transform>();
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 10; j++)
                    slots[i, j] = grid.GetChild(i).GetChild(j);
        }

        void CheckForWarnings() {
            if (playerTag == "")
                Debug.LogWarning("Type the player tag in the GameManager");

            if (enemyTag == "")
                Debug.LogWarning("Type the enemy tag in the GameManager");

        }

    }
}