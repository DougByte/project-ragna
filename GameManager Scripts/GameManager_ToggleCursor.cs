﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class GameManager_ToggleCursor : MonoBehaviour {

        private GameManager_Master gameManagerMaster;
        private bool isCursorLocked = true;

		void OnEnable(){
            SetInitialReferences();
            gameManagerMaster.MenuToggleEvent += ToggleCursorState;
            gameManagerMaster.InventiryUIToggleEvent += ToggleCursorState;
        }

		void OnDisable(){
            gameManagerMaster.MenuToggleEvent -= ToggleCursorState;
            gameManagerMaster.InventiryUIToggleEvent -= ToggleCursorState;
        }

        void Update () {
            CheckIfCursorShouldBeLocked();
		}

		void SetInitialReferences(){
            gameManagerMaster = GetComponent<GameManager_Master>();
		}

        void CheckIfCursorShouldBeLocked() {
            if (isCursorLocked) {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            } else {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        void ToggleCursorState() {
            isCursorLocked = !isCursorLocked;
        }
	}
}