﻿using UnityEngine;

namespace Ragna {

	public class GameManager_ToggleInventoryUI : MonoBehaviour {

        public GameObject inventoryInventoryUI;
        private string pcToggleInventory;
        //private string xboxToggleInventory;
        private GameManager_Master gameManagerMaster;

        void Start() {
            SetInitialReferences();
            gameManagerMaster.CallEventInventoryUIToggle();
            gameManagerMaster.CallEventInventoryUIToggle();
        }

        void Update(){
            CheckForToggleInventoryUIRequest();
		}

		void SetInitialReferences(){            
            gameManagerMaster = GetComponent<GameManager_Master>();
            pcToggleInventory = GameManager_ControllerManager.pcInventory;
            
        }

        void CheckForToggleInventoryUIRequest() {
            if (Input.GetButtonUp(pcToggleInventory) && !gameManagerMaster.isMenuOn && !gameManagerMaster.isGameOver) {
                ToggleInventoryUI();
            }
        }

        void ToggleInventoryUI() {
            if (inventoryInventoryUI != null) {
                inventoryInventoryUI.SetActive(!inventoryInventoryUI.activeSelf);
                gameManagerMaster.isInventoryUIOn = !gameManagerMaster.isInventoryUIOn;
                gameManagerMaster.CallEventInventoryUIToggle();
            }
        }
	}
}