﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class GameManager_ToggleMenu : MonoBehaviour {

        private GameManager_Master gameManagerMaster;
        public GameObject menu;

		void OnEnable(){
            SetInitialReferences();
            gameManagerMaster.GameOverEvent += ToggleMenu;
		}

		void OnDisable(){
            gameManagerMaster.GameOverEvent -= ToggleMenu;
        }

		void Update () {
            CheckForMenuToggleRequest();
		}

		void SetInitialReferences(){
            gameManagerMaster = GetComponent<GameManager_Master>();
		}

        void CheckForMenuToggleRequest() {
            if (Input.GetButtonUp("Start") && !gameManagerMaster.isGameOver)
                ToggleMenu();
        }

        void ToggleMenu() {
            menu.SetActive(!menu.activeSelf);
            gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
            gameManagerMaster.CallEventMenuToggle();
        }
	}
}