﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    public class GameManager_TogglePause : MonoBehaviour {

        private GameManager_Master gameManagerMaster;
        private bool isPaused;

        void OnEnable() {
            SetInitialReferences();
            gameManagerMaster.MenuToggleEvent += TogglePause;
            gameManagerMaster.InventiryUIToggleEvent += TogglePause;
        }

        void OnDisable() {
            gameManagerMaster.MenuToggleEvent -= TogglePause;
            gameManagerMaster.InventiryUIToggleEvent -= TogglePause;
        }

        void SetInitialReferences() {
            gameManagerMaster = GetComponent<GameManager_Master>();
        }

        void TogglePause() {
            if (isPaused && !gameManagerMaster.isInventoryUIOn) {
                Time.timeScale = 1;
                isPaused = false;
            } else {
                Time.timeScale = 0;
                isPaused = true;
            }
        }
    }
}