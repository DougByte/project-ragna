﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Item_Pickup : MonoBehaviour {

        private Item_Master itemMaster;

        void OnEnable() {
            SetInitialReferences();
            itemMaster.EventPickupAction += CarryOutPickupActions;
        }

        void OnDisable() {
            itemMaster.EventPickupAction -= CarryOutPickupActions;
        }

        void SetInitialReferences() {
            itemMaster = GetComponent<Item_Master>();
        }

        void CarryOutPickupActions(GameObject item) {
            Debug.Log("Chamado");
            GetComponent<Inventory_Drag>().parentToReturn = null;
            itemMaster.CallEventObjectPickup();
        }
    }
}