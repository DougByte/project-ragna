﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    [RequireComponent(typeof(Item_Master))]
    [RequireComponent(typeof(Rigidbody))]
	public class Item_Throw : MonoBehaviour {

        private Item_Master itemMaster;
        private Transform myTransform;
        private Vector3 throwDirection;
        public Transform direction;
        public string throwButtonName;
        public bool canBeThrown;
        public float throwForce;

		void Start () {
            SetInitialReferences();
        }
	
		void Update () {
            CheckForThrowInput();
		}

		void SetInitialReferences(){
            itemMaster = GetComponent<Item_Master>();
            myTransform = transform;
		}

        void CheckForThrowInput() {
            if (Input.GetButtonDown(throwButtonName) && Time.timeScale > 0 && canBeThrown
                && myTransform.root.CompareTag(GameManager_References.instance.playerTag)) {
                CarryOutThrowActions();
            }
        }

        void CarryOutThrowActions() {
            throwDirection = direction.forward;
            myTransform.parent = null;
            itemMaster.CallEventObjectThrow();
            hurlItem();
        }
        
        void hurlItem() {
            GetComponent<Rigidbody>().AddForce(throwDirection * throwForce, ForceMode.Impulse);
        }
	}
}