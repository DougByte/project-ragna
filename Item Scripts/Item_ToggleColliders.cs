﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Item_ToggleColliders : MonoBehaviour {

        private Item_Master itemMaster;
        private Collider[] colliders;

		void OnEnable(){
            SetInitialReferences();
            ToggleColliders();
            itemMaster.EventItemEquip += ToggleColliders;
            itemMaster.EventObjectDrop += ToggleColliders;
            itemMaster.EventObjectThrow += ToggleColliders;
        }

        void OnDisable() {
            itemMaster.EventItemEquip -= ToggleColliders;
            itemMaster.EventObjectDrop -= ToggleColliders;
            itemMaster.EventObjectThrow -= ToggleColliders;
        }

		void SetInitialReferences(){
            itemMaster = GetComponent<Item_Master>();
            colliders = GetComponentsInChildren<Collider>();
		}

        void ToggleColliders() {
            foreach (Collider coll in colliders) {
                coll.enabled = false;
                if (itemMaster.isEquiped && coll.isTrigger)
                    coll.enabled = true;
            }
        }
	}
}