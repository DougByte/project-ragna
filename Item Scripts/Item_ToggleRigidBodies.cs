﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Item_ToggleRigidbodies : MonoBehaviour {

        private Item_Master itemMaster;
        private Rigidbody[] rigidBodies;

        void OnEnable() {
            SetInitialReferences();
            ToggleRigidBody();
            itemMaster.EventItemEquip += ToggleRigidBody;
            itemMaster.EventObjectDrop += ToggleRigidBody;
            itemMaster.EventObjectThrow += ToggleRigidBody;
        }

        void OnDisable() {
            itemMaster.EventItemEquip -= ToggleRigidBody;
            itemMaster.EventObjectDrop -= ToggleRigidBody;
            itemMaster.EventObjectThrow -= ToggleRigidBody;
        }

        void SetInitialReferences() {
            itemMaster = GetComponent<Item_Master>();
            rigidBodies = GetComponentsInChildren<Rigidbody>();
        }

        void ToggleRigidBody() {
            foreach (Rigidbody rBody in rigidBodies) {
                if (itemMaster.isEquiped) {
                    rBody.isKinematic = true;
                    rBody.useGravity = false;
                } else {
                    rBody.isKinematic = false;
                    rBody.useGravity = true;
                }
            }
        }
    }
}