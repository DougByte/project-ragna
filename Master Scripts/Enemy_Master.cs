﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Enemy_Master : MonoBehaviour {

        [HideInInspector] public Transform target;
        [HideInInspector] public bool isOnRoute;
        [HideInInspector] public bool isNavPaused;
        [HideInInspector] public string self;

        public delegate void GeneralEventHandler();
        public event GeneralEventHandler EventEnemyDie;
        public event GeneralEventHandler EventEnemyWalking;
        public event GeneralEventHandler EventEnemyReachedNavTarget;
        public event GeneralEventHandler EventEnemyAttack;
        public event GeneralEventHandler EventEnemyLostTarget;

        public delegate void FloatEventHandler(float health);
        public event FloatEventHandler EventEnemyDeductHealth;
        public event FloatEventHandler EventEnemyFreeze;

        public delegate void NavTargetEventHandler(Transform targetTransform);
        public event NavTargetEventHandler EventEnemySetNavTarget;

        private void Start()
        {
            self = Random.Range(1, 10000000).ToString();
        }

        public void CallEventEnemyDeductHealth(float health) {
            if (EventEnemyDeductHealth != null)
                EventEnemyDeductHealth(health);
        }

        public void CallEventEnemyFreeze(float freezeTime) {
            if (EventEnemyFreeze != null)
                EventEnemyFreeze(freezeTime);
        }

        public void CallEventEventEnemySetNavTarget(Transform targetTransform) {
            if (EventEnemySetNavTarget != null)
                EventEnemySetNavTarget(targetTransform);
            target = targetTransform;
        }

        public void CallEventEnemyDie() {
            if (EventEnemyDie != null)
                EventEnemyDie();
        }

        public void CallEventEnemyWalking() {
            if (EventEnemyWalking != null)
                EventEnemyWalking();
        }

        public void CallEventEnemyReachedNavTarget() {
            if (EventEnemyReachedNavTarget != null)
                EventEnemyReachedNavTarget();
        }

        public void CallEventEnemyAttack() {
            if (EventEnemyAttack != null)
                EventEnemyAttack();
        }

        public void CallEventEnemyLostTarget() {
            if (EventEnemyLostTarget != null)
                EventEnemyLostTarget();
            target = null;
        }

    }
}