﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    public class GameManager_Master : MonoBehaviour {

        public delegate void GameManagerEventHandler();
        public event GameManagerEventHandler MenuToggleEvent;
        public event GameManagerEventHandler InventiryUIToggleEvent;
        public event GameManagerEventHandler GoToMenuSceneEvent;
        public event GameManagerEventHandler GameOverEvent;
        public event GameManagerEventHandler AddControllerEvent;

        public bool isGameOver;
        public bool isInventoryUIOn;
        public bool isMenuOn;
        public bool isMultiPlayer;

        public void CallEventMenuToggle() {
            if (MenuToggleEvent != null)
                MenuToggleEvent();
        }

        public void CallEventInventoryUIToggle() {
            if (InventiryUIToggleEvent != null)
                InventiryUIToggleEvent();
        }

        public void CallEventGoToMenuScene() {
            if (GoToMenuSceneEvent != null)
                GoToMenuSceneEvent();
        }

        public void CallEventGameOver() {
            if (GameOverEvent != null) {
                isGameOver = true;
                GameOverEvent();
            }
        }

        public void CallAddControllerEvent() {
            if (AddControllerEvent != null)
                AddControllerEvent();
        }

    }
}
