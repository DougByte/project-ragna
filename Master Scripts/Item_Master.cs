﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Item_Master : MonoBehaviour {

        private Player_Master playerMaster;
        public delegate void GeneralEventHandler();
        public event GeneralEventHandler EventObjectThrow;
        public event GeneralEventHandler EventObjectPickeup;
        public event GeneralEventHandler EventObjectDrop;
        public event GeneralEventHandler EventItemEquip;

        public delegate void PickeupActionEventHandler(GameObject inventory);
        public event PickeupActionEventHandler EventPickupAction;

        public GameObject item2D;
        public bool isEquiped;

        void OnEnable(){
            SetInitialReferences();
		}

		public void CallEventObjectThrow() {
            if (EventObjectThrow != null) {
                EventObjectThrow();
                playerMaster.CallEventInventoryChanged();
            }
        }

        public void CallEventObjectPickup() {
            if (EventObjectPickeup != null) {
                EventObjectPickeup();
                playerMaster.CallEventInventoryChanged();
            }
        }

        public void CallEventObjectDrop() {
            if (EventObjectDrop != null) 
                EventObjectDrop();
        }

        public void CallEventItemEquip() {
            if (EventItemEquip != null)
                EventItemEquip();
        }

        public void CallEventPickupAction(GameObject item) {
            if (EventPickupAction != null) 
                EventPickupAction(item);
        }

        void SetInitialReferences(){
            if (GameManager_References.instance.players != null)
                //Erro Aqui
                playerMaster = GameManager_References.instance.players[0].GetComponent<Player_Master>();
		}
	}
}