﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Melee_Master : MonoBehaviour {

        public delegate void GeneralEventHandler();
        public event GeneralEventHandler EventPlayerInput;
        public event GeneralEventHandler EventMeleReset;

        public delegate void MeleeHitEventHandler(Collider hitCol, Transform hitTransform);
        public event MeleeHitEventHandler EventHit;

        public bool isInUse = false;
        public float swingRate;

        public void CallEventPlayerInput() {
            if (EventPlayerInput != null)
                EventPlayerInput();
        }

        public void CallEventMeleReset() {
            if (EventMeleReset != null)
                EventMeleReset();
        }

        public void CallEventHit(Collider hitCol, Transform hitTransform) {
            if (EventHit != null)
                EventHit(hitCol, hitTransform);
        }
    }
}