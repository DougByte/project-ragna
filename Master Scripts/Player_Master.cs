﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    public class Player_Master : MonoBehaviour {

        public delegate void GeneralEventHandler();
        public event GeneralEventHandler EventInventoryChanged;
        public event GeneralEventHandler EventAmmoChanged;
        public event GeneralEventHandler EventPlayerSkillInput;
        public event GeneralEventHandler EventPlayerSkillAnimation;
        public event GeneralEventHandler EventSkillThrow;
        // EvHdlr(bool)
        public event GeneralEventHandler EventCameraLock;

        public delegate void AmmoPickpEventHandler(string ammoName, int quantity);
        public event AmmoPickpEventHandler EventPickedUpAmmo;

        public delegate void PlayerBasicStatsEventHandler(float statsChange);
        public event PlayerBasicStatsEventHandler EventPlayerHealthDeduction;
        public event PlayerBasicStatsEventHandler EventPlayerHealthIncrease;
        public event PlayerBasicStatsEventHandler EventPlayerManaDeduction;
        public event PlayerBasicStatsEventHandler EventPlayerManaIncrease;
        public event PlayerBasicStatsEventHandler EventPlayerStaminaDeduction;
        public event PlayerBasicStatsEventHandler EventPlayerStaminaIncrease;

        public delegate void AttackAniamationEventHandler(int index);
        public event AttackAniamationEventHandler EventPlayerAttack;

        public delegate void MovementAniamationEventHandler(float speed, bool jump);
        public event MovementAniamationEventHandler EventPlayerMoving;

        public int index = 0;
        public float moveSpeed;
        public bool useXboxController = true;
        public string playerIndex;
        public Camera cameraP;

        void OnEnable() {
            cameraP.gameObject.SetActive(true);
        }

        void OnDisable() {
            if(cameraP !=  null)
                cameraP.gameObject.SetActive(false);
        }

        public void CallEventPlayerMoving(float speed, bool jump = false) {
            if (EventPlayerMoving != null)
                EventPlayerMoving(speed, jump);
        }

        public void CallEventPlayerAttack(int index) {
            if (EventPlayerAttack != null)
                EventPlayerAttack(index);
        }

        public void CallEventInventoryChanged() {
            if (EventInventoryChanged != null)
                EventInventoryChanged();
        }

        public void CallEventAmmoChanged() {
            if (EventAmmoChanged != null)
                EventAmmoChanged();
        }

        public void CallEventPickedUpAmmo(string ammoName, int quantity) {
            if (EventPickedUpAmmo != null)
                EventPickedUpAmmo(ammoName, quantity);
        }

        public void CallEventPlayerHealthDeduction(float damage) {
            if (EventPlayerHealthDeduction != null) 
                EventPlayerHealthDeduction(damage);
        }

        public void CallEventPlayerHealthIncrease(float heal) {
            if (EventPlayerHealthIncrease != null)
                EventPlayerHealthIncrease(heal);
        }

        public void CallEventPlayerManaDeduction(float manaChange) {
            if (EventPlayerManaDeduction != null)
                EventPlayerManaDeduction(manaChange);
        }

        public void CallEventPlayerManaIncrease(float manaChange) {
            if (EventPlayerManaIncrease != null)
                EventPlayerManaIncrease(manaChange);
        }

        public void CallEventPlayerStaminaDeduction(float staminaChange) {
            if (EventPlayerStaminaDeduction != null)
                EventPlayerStaminaDeduction(staminaChange);
        }

        public void CallEventPlayerStaminaIncrease(float staminaChange) {
            if (EventPlayerStaminaIncrease != null)
                EventPlayerStaminaIncrease(staminaChange);
        }

        public void CallEventPlayerSkillInput() {
            if (EventPlayerSkillInput != null)
                EventPlayerSkillInput();
        }

        public void CallEventPlayerSkillAnimation() {
            if (EventPlayerSkillAnimation != null)
                EventPlayerSkillAnimation();
        }

        public void CallEventSkillThrow() {
            if (EventSkillThrow != null)
                EventSkillThrow();
        }

        public void CallEventCameraLock() {
            if(EventCameraLock != null) {
                EventCameraLock();
            }
        }
    }
}