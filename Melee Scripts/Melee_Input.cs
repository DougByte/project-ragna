﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Melee_Input : MonoBehaviour {

        private Melee_Master meleeMaster;
        private Player_Master playerMaster;
        private Transform myTransform;
        private Animator myAnimator;
        private string attackButtonName, controllerIndex, xboxAttack;

        void Start () {
            if (transform.parent == null) {
                enabled = false;
            } else { 
                SetInitialReferences();
            }
        }
	
		void Update () {
            CheckIfWaeponShouldAttack();
        }

		void SetInitialReferences() {
            meleeMaster = GetComponent<Melee_Master>();
            attackButtonName = GameManager_ControllerManager.pcRightHand;
            playerMaster = GetComponentInParent<Player_Master>();
            myTransform = transform;
            controllerIndex = GetComponentInParent<Player_Master>().playerIndex;
            xboxAttack = (controllerIndex + GameManager_ControllerManager.xboxX);
            myAnimator = GetComponentInParent<Animator>();
        }

        void CheckIfWaeponShouldAttack() {
            if(Time.timeScale > 0 && myTransform.root.CompareTag(GameManager_References.instance.playerTag) && !GameManager_References.instance.isAttacking[0]) {               
                if (((Input.GetButton(attackButtonName) && playerMaster.index == 0) || (Input.GetButton(xboxAttack) && playerMaster.useXboxController))) {
                    meleeMaster.CallEventPlayerInput();
                    meleeMaster.swingRate = GameManager_References.instance.currentAttackAnimationLength[0];
                }
            }
        }
	}
}