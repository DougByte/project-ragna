﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Melee_Strike : MonoBehaviour {

        private Melee_Master meleeMaster;
        public int damage = 25;

		void Start () {
            SetInitialReferences();
        }
	
		void OnTriggerEnter(Collider collision) {
            if (!collision.gameObject.CompareTag(GameManager_References.instance.playerTag) && collision.CompareTag(GameManager_References.instance.enemyTag)) {
                collision.transform.SendMessage("ProcessDamage", damage);
                meleeMaster.CallEventHit(collision, collision.transform);
            }
        }
        
		void SetInitialReferences(){
            meleeMaster = GetComponent<Melee_Master>();
		}
	}
}