﻿using UnityEngine;
using System.Collections.Generic;
using MEC;

namespace Ragna {

	public class Melee_Swing : MonoBehaviour {

        private Player_Master playerMaster;
        private Melee_Master meleeMaster;
        private Collider[] myColliders;
        private int currentAttack = 0;
        public int totalAttackAnimations = 2;

        void OnEnable(){
            Invoke("SetInitialReferences", 0.2f);
        }

		void OnDisable(){
            if(meleeMaster != null)
                meleeMaster.EventPlayerInput -= MeleeAttackActions;
        }
	
		void SetInitialReferences(){
            //Erro Aqui
            playerMaster = GetComponentInParent<Player_Master>();
            meleeMaster = GetComponent<Melee_Master>();
            myColliders = GetComponents<Collider>();
            if(transform.root.CompareTag(GameManager_References.instance.playerTag))
                foreach (Collider col in myColliders)
                    col.enabled = false;
            meleeMaster.EventPlayerInput += MeleeAttackActions;
        }

        void MeleeAttackActions() {
            if (!GameManager_References.instance.isAttacking[0]) {
                myColliders[0].enabled = true;
                Timing.RunCoroutine(_MeleeAttackComplete());
                currentAttack++;
                if (currentAttack > totalAttackAnimations)
                    currentAttack = 1;
                playerMaster.CallEventPlayerAttack(currentAttack);
            }
        }

        IEnumerator<float> _MeleeAttackComplete() {
            yield return Timing.WaitForSeconds(meleeMaster.swingRate);
            myColliders[0].enabled = false;
            meleeMaster.isInUse = false;
        }
	}
}