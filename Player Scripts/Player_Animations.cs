using UnityEngine;
using System.Collections.Generic;
using MEC;

namespace Ragna {

    public class Player_Animations : MonoBehaviour {

        private Player_Master playerMaster;
        private Animator myAnimator;

        void OnEnable() {
            Invoke("SetInitialReferences", 0.2f);
        }

        void OnDisable() {
            if (playerMaster != null) {
                playerMaster.EventPlayerAttack -= SetAnimationToAttack;
                playerMaster.EventPlayerMoving -= SetMovementAnimation;
            }
        }

        void SetInitialReferences() {
            playerMaster = GetComponentInParent<Player_Master>();
            myAnimator = GetComponent<Animator>();
            playerMaster.EventPlayerAttack += SetAnimationToAttack;
            playerMaster.EventPlayerMoving += SetMovementAnimation;
        }

        void SetAnimationToAttack(int indiceAnimation) {
            if (!GameManager_References.instance.isAttacking[0]) {
                myAnimator.SetFloat("currentAttack", indiceAnimation);
                myAnimator.SetTrigger("attack");
                GameManager_References.instance.isAttacking[0] = true;
                
                Timing.RunCoroutine(_WaitForCompleAttackAnimation());
            }
        }

        IEnumerator<float> _WaitForCompleAttackAnimation() {
            GameManager_References.instance.currentAttackAnimationLength[0] = myAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForSeconds(GameManager_References.instance.currentAttackAnimationLength[0] * 0.65f);
            GameManager_References.instance.isAttacking[0] = false;
        }

        void SetMovementAnimation(float speed, bool jump = false) {
            if (jump)
                myAnimator.SetTrigger("jump");
            else
                myAnimator.SetFloat("speed", speed);
        }

        void SetAnimationSpellToSpell() {
            playerMaster.CallEventSkillThrow();
        }

    }
}
