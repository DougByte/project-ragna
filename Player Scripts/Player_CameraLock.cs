﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ragna
{
    public class Player_CameraLock : MonoBehaviour
    {
        public float lockRange;
        public float unlockDistance;

        private Player_Master playerMaster;
        private Transform target;
        private Collider[] enemies;
        private int targetIndex = 0;
        private float input;        
        private bool isLocked = false;

        void Start() {
            playerMaster = GameManager_References.instance.players[0].GetComponent<Player_Master>();            
        }

        void LateUpdate() {
            if (isLocked) {
                transform.LookAt(target);
            }
            CheckForLockRequest();
        }

        void CheckForLockRequest() {
            input = Input.GetAxis(GameManager_ControllerManager.pcZoom);
            if (Time.timeScale > 0) {
                if (Input.GetButton(GameManager_ControllerManager.pcZoom) && !isLocked) {
                    enemies = Physics.OverlapSphere(GameManager_References.instance.players[0].transform.position, lockRange, GameManager_References.instance.enemyLayer);
                    if (enemies.Length > 0) {
                        QuickSortEnemies(enemies, 0, enemies.Length - 1);
                        targetIndex = 0;
                        target = enemies[targetIndex].transform;
                        isLocked = true;
                        playerMaster.CallEventCameraLock();
                    }
                } else if (target != null)
                    if ((Input.GetKey(KeyCode.P) || Vector3.Distance(target.transform.position, transform.position) > unlockDistance) && isLocked) {
                    isLocked = false;
                    playerMaster.CallEventCameraLock();
                } else if (isLocked && input != 0) {                    
                    targetIndex += 1 * ((input > 0) ? 1 : -1);

                    if (targetIndex == enemies.Length)
                        targetIndex = 0;
                    else if (targetIndex < 0)
                        targetIndex = enemies.Length - 1;

                    target = enemies[targetIndex].transform;
                }
            }
        }

        void QuickSortEnemies(Collider[] array, int left, int right) {
            int i, j, pivot;
            float pivotDistance;
            Vector3 playerPosition = GameManager_References.instance.players[0].transform.position;
            Collider aux;
            
            i = left;
            j = right;
            pivot = (int)((i + j) / 2);
            pivotDistance = Vector3.Distance(array[pivot].transform.position, playerPosition);

            while (i <= j) {
                while (Vector3.Distance(array[i].transform.position, playerPosition) < pivotDistance)
                    i++;
                while (Vector3.Distance(array[j].transform.position, playerPosition) > pivotDistance)
                    j--;
                if (i < j) {
                    aux = array[i];
                    array[i++] = array[j];
                    array[j--] = aux;
                } else {
                    if (i == j) {
                        i++;
                        j--;
                    }
                }
            }

            if (j > left)
                QuickSortEnemies(array, left, j);
            if (i < right)
                QuickSortEnemies(array, i, right);
        }

    }
}