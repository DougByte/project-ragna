﻿using UnityEngine;

namespace Ragna {

    public class Player_DetectItem : MonoBehaviour {
        //private GameManager_Master gameManagerMaster;
        public LayerMask layer;
        public GameObject text;
        private string actionKey;
        private string jActionKey;
        private bool itemInRange;
        private RectTransform inventory;
        private GameObject item;
        private RaycastHit hit;

        void Start() {
            inventory = GameManager_References.instance.inventory;
            actionKey = GameManager_ControllerManager.pcAction;
            jActionKey = GetComponentInParent<Player_Master>().playerIndex + GameManager_ControllerManager.xboxBack;
        }

        void Update() {
            CheckForNearItems();
            CheckForItemPickupAttempt(); 
        }

        void CheckForNearItems() {
            if (Physics.SphereCast(transform.position, 0.3f, transform.forward, out hit, 3f, layer) && hit.transform.root.tag != GameManager_References.instance.playerTag) {
                text.SetActive(true);
                item = hit.transform.GetComponentInChildren<Item_Master>().item2D;
                itemInRange = true;
            } else {
                text.SetActive(false);
                itemInRange = false;
            }
         }

        void CheckForItemPickupAttempt() {
            //PlayerMaster verificar o controle 
            if (Input.GetButtonDown(actionKey) && IsInventoryFree() && itemInRange && Time.timeScale > 0) {
                /*GameObject go =*/ Instantiate(item);
                //go.GetComponent<Item_Master>().CallEventPickupAction(item);
                Destroy(hit.transform.gameObject);
                itemInRange = false;
            }
        }

        bool IsInventoryFree() {
            if (item != null)
                return inventory.GetComponentInChildren<Inventory_Slot>().IsInventoryFree(item.GetComponent<Inventory_Drag>().slotHeight, item.GetComponent<Inventory_Drag>().slotWidth);
            return false;
        }

    }
}