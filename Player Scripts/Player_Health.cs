﻿using UnityEngine;

namespace Ragna {

    public class Player_Health : MonoBehaviour {
        private GameManager_Master gameManagerMaster;
        private Player_Master playerMaster;
        private Player_Stamina playerStamina;
        private Player_Mana playerMana;
        [SerializeField] private float maxHealth;
        public float MaxHealth { get { return maxHealth; } }
        [SerializeField] private float currentHealth;
        public float CurrentHealth { get { return currentHealth; }}
        public float hpRegenPerSec;
        private bool isRegenerating = false;
        public RectTransform life;
        private Vector3 lifeScale = Vector3.one;

        void OnEnable() {
            SetInitialReferences();
            playerMaster.EventPlayerHealthDeduction += DeductHealth;
            playerMaster.EventPlayerHealthIncrease += IncreaseHealth;
        }

        void OnDisable() {
            playerMaster.EventPlayerHealthDeduction -= DeductHealth;
            playerMaster.EventPlayerHealthIncrease -= IncreaseHealth;
        }

        void Update() {
            SetUI();

            if (hpRegenPerSec > 0 && currentHealth < maxHealth && playerStamina.CurrentStamina > 0) {
                IncreaseHealth(hpRegenPerSec);
            }
        }

        void SetInitialReferences() {
            gameManagerMaster = GameObject.Find("GameManager").GetComponent<GameManager_Master>();
            playerMaster = GetComponent<Player_Master>();
            playerStamina = GetComponent<Player_Stamina>();
            playerMana = GetComponent<Player_Mana>();
        }

        void SetUI() {
            lifeScale.x = currentHealth / maxHealth;
            life.localScale = lifeScale;//Vector3.Lerp(life.localScale, lifeScale, Time.deltaTime);
            playerStamina.stamina.localPosition = new Vector3(-life.rect.width / 2 * lifeScale.x, playerStamina.stamina.localPosition.y);//Vector3.Lerp(playerStamina.stamina.localPosition, new Vector3(-life.rect.width / 2 * lifeScale.x, playerStamina.stamina.localPosition.y), Time.deltaTime);
            playerMana.mana.localPosition = new Vector3(life.rect.width / 2 * lifeScale.x, playerMana.mana.localPosition.y);//Vector3.Lerp(playerMana.mana.localPosition, new Vector3(life.rect.width / 2 * lifeScale.x, playerMana.mana.localPosition.y), Time.deltaTime);
        }

        void DeductHealth(float healthChange) {
            currentHealth -= healthChange;
            if (currentHealth <= 0) {
                currentHealth = 0;
                gameManagerMaster.CallEventGameOver();
            }            
        }

        void IncreaseHealth(float healthChange) {
            currentHealth += (healthChange * Time.deltaTime);
            if (currentHealth >= maxHealth) {
                currentHealth = maxHealth;
            }
        }
    }
}