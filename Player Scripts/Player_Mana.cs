﻿using UnityEngine;

namespace Ragna {

    public class Player_Mana : MonoBehaviour {

        private Player_Master playerMaster;
        [SerializeField] private float maxMana;
        [SerializeField] private float currentMana;
        public float MaxMana { get { return maxMana; } }
        public float CurrentMana { get { return currentMana; } }
        public float mpRegenPerSec;
        public RectTransform mana;
        public string[] manaInputs = new string[4];
        private Vector3 manaScale = Vector3.one;

        void OnEnable(){
            SetInitialReferences();
            playerMaster.EventPlayerManaDeduction += DeductMana;
            playerMaster.EventPlayerManaIncrease += IncreaseMana;
        }

        void OnDisable() {
            playerMaster.EventPlayerManaDeduction -= DeductMana;
            playerMaster.EventPlayerManaIncrease -= IncreaseMana;
        }

        void Update() {
            SetUI();

            if (mpRegenPerSec > 0 && currentMana < maxMana && !Input.GetButton(playerMaster.playerIndex + GameManager_ControllerManager.xboxY) && !Input.GetButton(manaInputs[0]) && !Input.GetButton(manaInputs[1]) && !Input.GetButton(manaInputs[2]) && !Input.GetButton(manaInputs[3])) {
                IncreaseMana(mpRegenPerSec);
            }
        }

        void SetInitialReferences(){
            playerMaster = GetComponent<Player_Master>();
        }        

        void SetUI() {
            manaScale.x = currentMana / maxMana;
            mana.localScale = manaScale;            
        }

        void DeductMana(float manaChange) {
            currentMana -= manaChange;
            if (currentMana <= 0) {
                currentMana = 0;
            }
        }

        void IncreaseMana(float manaChange) {
            currentMana += (manaChange * Time.deltaTime);
            if (currentMana >= maxMana) {
                currentMana = maxMana;
            }
        }
    }
}