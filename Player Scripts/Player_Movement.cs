﻿using UnityEngine;

namespace Ragna {

    public class Player_Movement : MonoBehaviour {

        private Player_Master playerMaster;
        public float jumpSpeed = 8f;
        public float gravity = 20f;
        public float pushPower = 2.0F;
        public float turnSmoothTime = 0.2f;
        private float turnSmoothVelocity;
        private float targetRotation;
        private Vector3 moveDirection = Vector3.zero;
        private Player_Animations animationContoller;
        private CharacterController characterController;
        private float horizontalMoveSpeed;
        private float verticalMoveSpeed;
        private float kbV = 0, kbH = 0, xbV, xbH;
        public Transform cameraT;
        public float speedSmoothTime = 0.1f;
        private float speedSmoothVelocity;
        private float currentSpeed;

        void Start() {
            SetInitialReferences();
        }

        void Update() {
            Move();
            SetAnimation();
        }

        void SetInitialReferences() {
            playerMaster = GetComponent<Player_Master>();
            characterController = GetComponent<CharacterController>();
        }

        void Move() {
            if (characterController.isGrounded) {
                xbH = 0;
                xbV = 0;
                if (playerMaster.useXboxController) {                    
                    xbV = Input.GetAxis(playerMaster.playerIndex + GameManager_ControllerManager.xboxVertical);
                    xbH = Input.GetAxis(playerMaster.playerIndex + GameManager_ControllerManager.xboxHorizontal);
                }
                if (playerMaster.index == 0) {
                    kbV = Input.GetAxis(GameManager_ControllerManager.pcVertical);
                    kbH = Input.GetAxis(GameManager_ControllerManager.pcHorizontal);
                }
                verticalMoveSpeed = kbV + xbV;
                horizontalMoveSpeed = kbH + xbH;
                verticalMoveSpeed = Mathf.Clamp(verticalMoveSpeed, -1, 1);
                horizontalMoveSpeed = Mathf.Clamp(horizontalMoveSpeed, -1, 1);

                if (horizontalMoveSpeed != 0 || verticalMoveSpeed != 0) {
                    targetRotation = Mathf.Atan2(horizontalMoveSpeed, verticalMoveSpeed) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
                    transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
                }

                currentSpeed = Mathf.SmoothDamp(currentSpeed, new Vector2(verticalMoveSpeed, horizontalMoveSpeed).normalized.magnitude * playerMaster.moveSpeed, ref speedSmoothVelocity, speedSmoothTime);

                moveDirection = transform.forward * currentSpeed;

                CheckIfShouldJump();
            }

            moveDirection.y -= gravity * Time.deltaTime;
            characterController.Move(moveDirection * Time.deltaTime);
        }

        void SetAnimation() {
            playerMaster.CallEventPlayerMoving(playerMaster.moveSpeed);
        }

        void CheckIfShouldJump() {
            if ((Input.GetButton(GameManager_ControllerManager.pcJump) 
                && playerMaster.index == 0)
                || (Input.GetButton(playerMaster.playerIndex + GameManager_ControllerManager.xboxA)
                && playerMaster.useXboxController)) {
                playerMaster.CallEventPlayerMoving(0, true);
                moveDirection.y = jumpSpeed;

            }
        }

        void OnControllerColliderHit(ControllerColliderHit hit) {
            Rigidbody hitRigidbody = hit.collider.attachedRigidbody;

            if (hitRigidbody == null || hitRigidbody.isKinematic)
                return;

            if (hit.moveDirection.y < -0.3f)
                return;

            Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
            hitRigidbody.velocity = pushDir * pushPower;
        }

    }
}