﻿using UnityEngine;
using MEC;
using System.Collections;
using System.Collections.Generic;

namespace Ragna {

	public class Player_Stamina : MonoBehaviour {

        private Player_Master playerMaster;
        private Player_Health playerHealth;
        [SerializeField] private float maxStamina;
        [SerializeField] private float currentStamina;
        public float MaxStamina { get { return maxStamina; } }
        public float CurrentStamina { get { return currentStamina; } }
        public float spRegenPerSec;
        private float spRegenRate = 1f;
        private bool isRegenerating = false;
        private float spWait;
        public RectTransform stamina;
        private Vector3 staminaScale = Vector3.one;
        //Movement
        public float runCost = 3f;
        public float speedRun;
        public float speedWalk;

        void OnEnable(){
            SetInitialReferences();
            playerMaster.EventPlayerStaminaDeduction += DeductStamina;
            playerMaster.EventPlayerStaminaIncrease += IncreaseStamina;
        }

        void OnDisable() {
            playerMaster.EventPlayerStaminaDeduction -= DeductStamina;
            playerMaster.EventPlayerStaminaIncrease -= IncreaseStamina;
        }

        void Update() {
            SetUI();
            SetMovementSpeed();            

            if (spRegenPerSec > 0 && currentStamina < maxStamina && !Input.GetButton(GameManager_ControllerManager.pcRun) && !Input.GetButton(playerMaster.playerIndex + GameManager_ControllerManager.xboxLS)) {
                IncreaseStamina(spRegenPerSec);
            }
        }
        void SetInitialReferences(){
            playerMaster = GetComponent<Player_Master>();
            playerHealth = GetComponent<Player_Health>();
        }

        void SetUI() {
            staminaScale.x = currentStamina / maxStamina;
            stamina.localScale = staminaScale;
        }

        void DeductStamina(float staminaChange) {
            currentStamina -= staminaChange;
            if (currentStamina <= 0) {
                currentStamina = 0;
            }
        }

        void IncreaseStamina(float staminaChange) {
            currentStamina += (staminaChange * Time.deltaTime);
            if (currentStamina >= maxStamina) {
                currentStamina = maxStamina;
            }
        }

        void SetMovementSpeed() {
            if (((Input.GetAxis(GameManager_ControllerManager.pcVertical) != 0
                || Input.GetAxis(GameManager_ControllerManager.pcHorizontal) != 0)
                && playerMaster.index == 0)
                 || (playerMaster.useXboxController
                 && (Input.GetAxis(playerMaster.playerIndex + GameManager_ControllerManager.xboxHorizontal) != 0
                 || Input.GetAxis(playerMaster.playerIndex + GameManager_ControllerManager.xboxVertical) != 0)))
            {
                playerMaster.moveSpeed = speedWalk;

                if ((Input.GetButton(playerMaster.playerIndex + GameManager_ControllerManager.xboxLS)
                    && playerMaster.useXboxController)
                    || Input.GetButton(GameManager_ControllerManager.pcRun))
                {
                    if (CurrentStamina > 0)
                    {
                        DeductStamina(runCost * Time.deltaTime);
                        playerMaster.moveSpeed = speedRun;
                    }
                    else if (playerHealth.CurrentHealth > (playerHealth.MaxHealth * 0.2f))
                    {
                        playerMaster.CallEventPlayerHealthDeduction(runCost * 1.35f * Time.deltaTime);
                        playerMaster.moveSpeed = speedRun * 0.6f;
                    }
                }
            } else
            {
                playerMaster.moveSpeed = 0;
            }        
        }
    }
}