﻿using UnityEngine;

namespace Ragna {

    public class Player_Status : MonoBehaviour {

        //private GameManager_Master gameManagerMaster;
        //private Player_Master playerMaster;

        public int strength;  
        public int agility;   
        public int arcana;

        private int currentLevel = 1;
        private float currentExp;

        public int xpMultiply = 1;
        public float xpToLevelUp = 100.0f;
        public float difficultFactor = 1.5f;
        
        void OnEnable() {
            SetInitialReferences();
        }
        
        /*
        void OnDisable() {

        }

        void Update() {
            SetUI();
            Creio q SetUI() seria melhor chamado pelo EventPlayerLevelUP;
            ou coisa parecida.
        }
        */

        void SetInitialReferences() {
           // gameManagerMaster = GameObject.Find("GameManager").GetComponent<GameManager_Master>();
           // playerMaster = GetComponent<Player_Master>();
        }            

        public void AddXp(float xpAdd) {
            float newExp = (currentExp + xpAdd) * xpMultiply;
            while (newExp >= GetNextXp()) {
                newExp -= GetNextXp();
                currentLevel++;
            }
            currentExp = newExp;
        }

        public float GetNextXp() {
            return ((currentLevel ^ 2) + xpToLevelUp * (currentLevel + 1)) * difficultFactor;
        }

    }
}