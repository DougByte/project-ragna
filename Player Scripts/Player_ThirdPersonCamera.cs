﻿using UnityEngine;
using System.Collections.Generic;
using MEC;

namespace Ragna {

	public class Player_ThirdPersonCamera : MonoBehaviour {

        public float sensitivity = 6;
        public Transform target;
        public Vector2 verticalMinMax = new Vector2(-40, 85);
        public float rotationSmoothTime = 0.05f;

        private Player_Master playerMaster;
        private bool isLocked = false;
        private float distFromTarget = 3;
        private float horizontal;
        private float vertical;
        private string controllerIndex;
        private string xboxHorizontal;
        private string xboxVertical;
        private string xboxZoom;
        private Vector3 rotationSmoothVelocity;
        private Vector3 currentRotation;

        void Start(){            
            xboxHorizontal = "J1_" + GameManager_ControllerManager.xboxXAxis;
            xboxVertical = "J1_" + GameManager_ControllerManager.xboxYAxis;
            xboxZoom = "J1_" + GameManager_ControllerManager.xboxTriggers;
            Timing.RunCoroutine(_SetInitialReferences());
        }

        void OnDisable() {
            GameManager_References.instance.gameManagerMaster.AddControllerEvent -= RenderSize;
            playerMaster.EventCameraLock -= SetLock;
        }

        void LateUpdate () {
            if (Time.timeScale > 0 && !isLocked)
            {

                if (target.GetComponentInParent<Player_Master>().index == 0 && target.GetComponentInParent<Player_Master>().useXboxController)
                {
                    horizontal += (Input.GetAxis(GameManager_ControllerManager.pcXAxis) + Input.GetAxis(xboxHorizontal)) * sensitivity;
                    vertical -= (Input.GetAxis(GameManager_ControllerManager.pcYAxis) + Input.GetAxis(xboxVertical)) * sensitivity;
                    distFromTarget -= (Input.GetAxis(GameManager_ControllerManager.pcZoom) + Input.GetAxis(xboxZoom));
                }
                else if (target.GetComponentInParent<Player_Master>().index == 0)
                {
                    horizontal += Input.GetAxis(GameManager_ControllerManager.pcXAxis) * sensitivity;
                    vertical -= Input.GetAxis(GameManager_ControllerManager.pcYAxis) * sensitivity;
                    if (Input.GetKey(KeyCode.LeftAlt))
                        distFromTarget -= Input.GetAxis(GameManager_ControllerManager.pcZoom);
                }
                else
                {
                    horizontal += Input.GetAxis(xboxHorizontal) * sensitivity;
                    vertical += Input.GetAxis(xboxVertical) * sensitivity;
                    distFromTarget -= Input.GetAxis(xboxZoom);
                }

                vertical = Mathf.Clamp(vertical, verticalMinMax.x, verticalMinMax.y);

                currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(vertical, horizontal), ref rotationSmoothVelocity, rotationSmoothTime);
                transform.eulerAngles = currentRotation;
            }

            if (isLocked) {
                vertical = transform.eulerAngles.x;
                horizontal = transform.eulerAngles.y;
            }

            distFromTarget = Mathf.Clamp(distFromTarget, 2, 5);
            transform.position = target.position - transform.forward * distFromTarget;
        }

		IEnumerator<float> _SetInitialReferences(){
            GameManager_References.instance.gameManagerMaster.AddControllerEvent += RenderSize;
            playerMaster = GameManager_References.instance.players[0].GetComponent<Player_Master>();
            playerMaster.EventCameraLock += SetLock;
            yield return Timing.WaitForSeconds(0.001f);
            controllerIndex = target.GetComponentInParent<Player_Master>().playerIndex;
            xboxHorizontal = (controllerIndex + GameManager_ControllerManager.xboxXAxis);
            xboxVertical = (controllerIndex + GameManager_ControllerManager.xboxYAxis);
            xboxZoom = (controllerIndex + GameManager_ControllerManager.xboxTriggers);
        }

        void SetLock() {
            isLocked = !isLocked;
            
        }

        void RenderSize(){
            
            if (target.GetComponentInParent<Player_Master>().index == 2 || target.GetComponentInParent<Player_Master>().index == 3)
                return;

            float x, y, w, h;

            if (GameManager_References.instance.players[2].activeSelf && target.GetComponentInParent<Player_Master>().index == 1) {
                x = 0.5f;
                y = 0.5f;
                w = 0.5f;
                h = 0.5f;
            } else if (GameManager_References.instance.players[2].activeSelf) {
                x = 0;
                y = 0.5f;
                w = 0.5f;
                h = 0.5f;
            } else if (GameManager_References.instance.players[1].activeSelf && target.GetComponentInParent<Player_Master>().index == 0) {
                x = 0;
                y = 0.5f;
                w = 1;
                h = 0.5f;
            } else if (GameManager_References.instance.players[1].activeSelf) {
                x = 0;
                y = 0;
                w = 1;
                h = 0.5f;
            } else {
                x = 0;
                y = 0;
                w = 1;
                h = 1;
            }

            GetComponent<Camera>().rect = new Rect(x, y, w, h);
        }
	}
}