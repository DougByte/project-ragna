﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScale : MonoBehaviour {

    public float scale = 8f;

    void Start()
    {
        GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(scale, scale));
    }

}
