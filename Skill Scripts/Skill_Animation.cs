﻿using UnityEngine;
using MEC;
using System.Collections;
using System.Collections.Generic;

namespace Ragna {

	public class Skill_Animation : MonoBehaviour {

        private Player_Master playerMaster;
        private Animator myAnimator;
        public string stateName;

		void Start(){
            SetInitialReferences();
            playerMaster.EventPlayerSkillAnimation += SetAnimation;
        }

		void OnDisable(){
            playerMaster.EventPlayerSkillAnimation -= SetAnimation;
        }

        void SetInitialReferences(){
            playerMaster = GetComponentInParent<Player_Master>();
            myAnimator = GetComponentInParent<Animator>();
        }

        void SetAnimation() {
            myAnimator.Play(stateName);
            StartCoroutine(_WaitForAnimationStart());
        }

        IEnumerator _WaitForAnimationStart() {
            //Aparentemente a animação só inicia após 2 frames
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            Timing.RunCoroutine(_CarryOutThrowActions());
        }

        IEnumerator<float> _CarryOutThrowActions() {
            yield return Timing.WaitForSeconds(myAnimator.GetCurrentAnimatorStateInfo(0).length * 0.8f);
            playerMaster.CallEventSkillThrow();
        }
	}
}