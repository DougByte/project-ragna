﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Skill_AreaDamage : MonoBehaviour {

        private Collider[] targets;
        public bool shouldFreeze;
        public float freezeTime;
        public int damage;
        public float radius;
        public LayerMask target;

        void OnEnable() {
            SetInitialReferences();
            ApplyDamage();
        }

        void SetInitialReferences(){
            targets = Physics.OverlapSphere(transform.position, radius, target);
        }

        void ApplyDamage() {
            foreach (Collider col in targets) {
                    col.SendMessage("ProcessDamage", damage);
                    if (shouldFreeze)
                        col.SendMessage("Freeze", freezeTime);
            }
        }
	}
}