﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    public class Skill_ContactDamage : MonoBehaviour {

        public bool shouldFreeze;
        public float freezeTime;
        public int damage;
        public Transform secondarySFXPrefab;

        private static Quaternion startRotation = Quaternion.identity;

        void OnEnable() {
            if (startRotation == Quaternion.identity)
                startRotation = transform.rotation;

            transform.localPosition = Vector3.zero;
            transform.rotation = startRotation;
        }

        void OnCollisionEnter(Collision collision) {
            if (Time.timeScale > 0 && !collision.transform.root.CompareTag(GameManager_References.instance.playerTag) && !transform.root.CompareTag(GameManager_References.instance.playerTag)) {
                ContactPoint contact = collision.contacts[0];
                Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
                Vector3 pos = contact.point;

                if (secondarySFXPrefab != null) {
                    Transform sfx = Instantiate(secondarySFXPrefab, pos, rot) as Transform;
                    Destroy(sfx.gameObject, sfx.GetComponentInChildren<ParticleSystem>().duration + 0.01f);
                }

                if (collision.transform.root.CompareTag(GameManager_References.instance.enemyTag)) {
                    collision.transform.SendMessage("ProcessDamage", damage);
                    if (shouldFreeze) {
                        collision.transform.SendMessage("Freeze", freezeTime);
                    }
                }

                Debug.Log(collision.gameObject.name);
                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<Collider>().enabled = false;
                gameObject.SetActive(false);
            }
        }
    }
}