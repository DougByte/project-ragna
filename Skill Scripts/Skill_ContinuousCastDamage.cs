﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Skill_ContinuousCastDamage : MonoBehaviour {

        public float damage;
        //public float distance;

        void OnParticleCollision(GameObject other) {
            if (Time.timeScale > 0 && other.transform.root.CompareTag(GameManager_References.instance.enemyTag)) {
                other.SendMessage("ProcessDamage", damage*Time.deltaTime);
                Debug.Log(other.name);
            }
        }
    }
}