﻿using UnityEngine;

namespace Ragna {

	public class Skill_Input : MonoBehaviour {

        private Player_Master playerMaster;
        private Player_Mana playerMana;
        private Animator myAnimator;
        private float nextSkillCast;
        private bool isCasting = false;
        private string xboxCasting;

        [Tooltip("The time between one fireball and other, 0 if it's a flame thrower")]
        public float skillCastRate;
        public float manaConsumption;

        void Start(){
            SetInitialReferences();
		}
	
		void Update () {
            CheckForSkillInputAttempt();
		}

		void SetInitialReferences(){
            playerMaster = GetComponentInParent<Player_Master>();
            playerMana = GetComponentInParent<Player_Mana>();
            myAnimator = GetComponentInParent<Animator>();
            xboxCasting = playerMaster.playerIndex + GameManager_ControllerManager.xboxY;
        }

        void CheckForSkillInputAttempt() {
            if (Time.timeScale > 0 && Time.time > nextSkillCast && playerMana.CurrentMana >= manaConsumption && transform.root.CompareTag(GameManager_References.instance.playerTag)) {
                if (((Input.GetButtonDown(GameManager_ControllerManager.pcLeftHand)
                    && playerMaster.index == 0)
                    || (Input.GetButtonDown(xboxCasting)
                    && playerMaster.useXboxController)) 
                    && skillCastRate != 0) {
                    nextSkillCast = Time.time + skillCastRate;
                    playerMaster.CallEventPlayerManaDeduction(manaConsumption);
                    if (!isCasting) {
                        isCasting = true;
                        myAnimator.SetBool("isCasting", isCasting);
                        playerMaster.CallEventPlayerSkillInput();
                    }
                } else if (((Input.GetButtonDown(GameManager_ControllerManager.pcLeftHand) && playerMaster.index == 0)
                    || (Input.GetButtonDown(xboxCasting)  && playerMaster.useXboxController)) && skillCastRate == 0) {
                            playerMaster.CallEventPlayerManaDeduction(manaConsumption * Time.deltaTime);
                            isCasting = true;
                            myAnimator.SetBool("isCasting", isCasting);
                            playerMaster.CallEventPlayerSkillInput();
                       }
            }

            if (((Input.GetButtonDown(GameManager_ControllerManager.pcLeftHand)
                    && playerMaster.index == 0)
                    || (Input.GetButtonDown(xboxCasting)
                    && playerMaster.useXboxController))) {
                isCasting = false;
                myAnimator.SetBool("isCasting", isCasting);
            }
        }
    }
}