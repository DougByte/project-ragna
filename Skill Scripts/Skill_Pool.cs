﻿using UnityEngine;
using System.Collections;

namespace Ragna {

	public class Skill_Pool : MonoBehaviour {

        private Player_Master playerMaster;
        public GameObject[] spells;
        private Transform objectToSpawn;

		void Start(){
            SetInitialReferences();
            playerMaster.EventPlayerSkillInput += EnableSpell;

        }

		void OnDisable(){
            playerMaster.EventPlayerSkillInput -= EnableSpell;
        }
	
		void SetInitialReferences(){
            playerMaster = GetComponentInParent<Player_Master>();
            objectToSpawn = spells[0].transform;
		}

        void EnableSpell() {
            foreach (GameObject go in spells) {
                if (go.activeSelf && go.transform.parent == transform)
                    return;
                if (!go.activeSelf) {
                    go.transform.parent = transform;
                    go.SetActive(true);
                    playerMaster.CallEventPlayerSkillAnimation();
                    return;
                }
            }
            Transform spell = Instantiate(objectToSpawn, transform) as Transform;
            playerMaster.CallEventPlayerSkillAnimation();
            Destroy(spell.gameObject, 15f);
        }
	}
}