﻿using UnityEngine;
using System.Collections;

namespace Ragna {

    [RequireComponent(typeof(Rigidbody))]
	public class Skill_Throw : MonoBehaviour {

        private Player_Master playerMaster;
        private Transform myTransform;        
        private Vector3 throwDirection;
        public float throwForce;

        void OnEnable() {
            SetInitialReferences();
            playerMaster.EventSkillThrow += CarryOutThrowActions;
        }

        void OnDisable() {
            playerMaster.EventSkillThrow -= CarryOutThrowActions;
        }

		void SetInitialReferences(){
            playerMaster = GetComponentInParent<Player_Master>();
            myTransform = GetComponent<Transform>();
		}

        void CarryOutThrowActions() {
            if (transform.parent != null) {
                throwDirection = GetComponentInParent<Player_Master>().cameraP.transform.forward;
                myTransform.parent = null;
                hurlItem();
            }
        }

        void hurlItem() {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().AddForce(throwDirection * throwForce, ForceMode.Impulse);
            GetComponent<Collider>().enabled = true;
        }
    }
}