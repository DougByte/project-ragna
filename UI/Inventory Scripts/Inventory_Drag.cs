﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum Slot { INVENTORY, WEAPON, HEAD, CHEST, ARMS, LEGS, FEET};

namespace Ragna {

    public class Inventory_Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

        [HideInInspector] public Transform parentToReturn;
        [HideInInspector] public GameObject spawnedItem = null;

        [Range(1, 5)] public int slotHeight;
        [Range(1, 5)] public int slotWidth;

        public Slot itemType;
        public GameObject item;

        private Transform startLine;
        private Transform endLine;
        private Inventory_Slot dropZone;
        private Vector3 globalMousePosition;

        void OnEnable()
        {
            SetInitialReferences();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData.button != 0) return;

            if (spawnedItem != null)
            {
                Destroy(spawnedItem);
                spawnedItem = null;
            }

            GetComponent<CanvasGroup>().blocksRaycasts = false;
            if (parentToReturn != null)
                if (parentToReturn.tag == "Inventory")
                    dropZone.SetState(parentToReturn.parent.GetSiblingIndex(), parentToReturn.GetSiblingIndex(), eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotHeight, eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotWidth, Color.white, true);
                else
                    parentToReturn.GetComponent<Image>().color = Color.white;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.button != 0)
                return;
            Cursor.visible = false;

            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(GetComponent<RectTransform>(), eventData.position, eventData.pressEventCamera, out globalMousePosition))
                transform.position = globalMousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData.button != 0)
                return;
            Cursor.visible = true;
            if (!eventData.pointerCurrentRaycast.isValid)
            {
                if (DropItem())
                    Destroy(gameObject);
            }
            else
            {
                transform.position = parentToReturn.position;

                if (parentToReturn.tag == "Inventory")
                    dropZone.SetState(parentToReturn.parent.GetSiblingIndex(), parentToReturn.GetSiblingIndex(), eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotHeight, eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotWidth, Color.gray, true);
                else
                    parentToReturn.GetComponent<Image>().color = Color.gray;
                GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
        }

        void SetInitialReferences()
        {
            transform.SetParent(GameManager_References.instance.inventory.Find("Items"), false);
            dropZone = GameManager_References.instance.inventory.GetComponentInChildren<Inventory_Slot>();
            if (parentToReturn == null)
                dropZone.FindSlot(slotHeight, slotWidth, this);
            startLine = GameObject.Find("StartLine").GetComponent<Transform>();
            endLine = GameObject.Find("EndLine").GetComponent<Transform>();
        }

        public bool DropItem()
        {
            int count = 0;
            Quaternion originalRotation = startLine.localRotation;
            while (Physics.Linecast(startLine.position, endLine.position) || count == 15)
            {
                startLine.Rotate(0, 24, 0);
                count++;
            }
            if (count == 15) return false;

            item.GetComponentInChildren<Rigidbody>().isKinematic = false;
            Instantiate(item, endLine.position, endLine.rotation);
            startLine.localRotation = originalRotation;
            return true;
        }

    }
}
