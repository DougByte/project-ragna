﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Ragna
{
    public class Inventory_Slot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Slot itemType = Slot.INVENTORY;
        public Transform whereSpawnItem;

        private int lin, col, height, width, normalizedCol;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) return;

            SetSlotsVar(eventData);
            if (transform.tag == "Inventory")
                SetState(lin, col, height, width, Color.yellow);
            else if (transform.tag == "Equipment")
                transform.GetComponent<Image>().color = Color.yellow;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) return;

            SetSlotsVar(eventData);
            if (transform.tag == "Inventory")
                SetState(lin, col, height, width, Color.white);
            else if (transform.tag == "Equipment")
                transform.GetComponent<Image>().color = Color.white;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.button != 0) return;

            Inventory_Drag d = eventData.pointerDrag.GetComponent<Inventory_Drag>();

            if (d != null)
            {
                SetSlotsVar(eventData);
                if (itemType == d.itemType)
                {
                    d.parentToReturn = transform;
                    d.item.GetComponentInChildren<Rigidbody>().isKinematic = true;
                    d.spawnedItem = Instantiate(d.item, whereSpawnItem.position, whereSpawnItem.rotation, whereSpawnItem) as GameObject;
                    d.spawnedItem.GetComponent<Rigidbody>().useGravity = false;
                }
                else if (GameManager_References.instance.slots[NormalizedLin(lin, height), normalizedCol].GetComponent<Image>().color == Color.yellow)
                    d.parentToReturn = GameManager_References.instance.slots[NormalizedLin(lin, height), normalizedCol];

                if (transform.tag == "Equipment" && d.parentToReturn != transform)
                    transform.GetComponent<Image>().color = Color.white;
            }
        }

        public bool IsInventoryFree(int slotHeight, int slotWidth)
        {
            foreach (Transform t in GameManager_References.instance.slots)
            {
                lin = t.parent.GetSiblingIndex();
                col = t.GetSiblingIndex();
                normalizedCol = NormalizedCol(col, slotWidth);
                lin = NormalizedLin(lin, slotHeight);
                if (!IsInUse(lin, normalizedCol, slotHeight, slotWidth))
                    return true;
            }
            return false;
        }

        public bool IsInUse(int lin, int col, int slotHeight, int slotWidth)
        {
            for (int i = 0; i < slotHeight; i++)
                for (int j = 0; j < slotWidth; j++)
                    if (GameManager_References.instance.slots[lin + i, col + j].GetComponent<Image>().color == Color.grey)
                        return true;
            return false;
        }

        public void FindSlot(int slotHeight, int slotWidth, Inventory_Drag d)
        {
            if (IsInventoryFree(slotHeight, slotWidth))
            {
                d.parentToReturn = GameManager_References.instance.slots[NormalizedLin(lin, slotHeight), normalizedCol];
                d.transform.position = d.parentToReturn.position;
                SetState(lin, normalizedCol, slotHeight, slotWidth, Color.gray);
            }
        }

        public void SetState(int lin, int col, int slotHeight, int slotWidth, Color state, bool ignoreInUse = false)
        {
            normalizedCol = NormalizedCol(col, slotWidth);
            lin = NormalizedLin(lin, slotHeight);

            if ((ignoreInUse) || (!IsInUse(lin, normalizedCol, slotHeight, slotWidth)))
                for (int i = 0; i < slotHeight; i++)
                    for (int j = 0; j < slotWidth; j++)
                        GameManager_References.instance.slots[lin + i, normalizedCol + j].GetComponent<Image>().color = state;
        }

        void SetSlotsVar(PointerEventData eventData)
        {
            lin = transform.parent.GetSiblingIndex();
            col = transform.GetSiblingIndex();
            height = eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotHeight;
            width = eventData.pointerDrag.transform.GetComponent<Inventory_Drag>().slotWidth;
        }

        public int NormalizedCol(int col, int width)
        {
            if(col + width > 10)
                col -= (col + width) - 10; ;
            return col;
        }

        public int NormalizedLin(int lin, int higth)
        {
            if(lin + higth > 5)
                lin -= (lin + higth) - 5;
            return lin;
        }
    }
}