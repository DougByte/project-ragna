﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ragna {
    public class ShowFPS : MonoBehaviour {

        int frames = 0;
        float sec = 1f;
        int fps;
        Text text;

        void Start() {
            text = GetComponent<Text>();
        }

        void Update() {
            if(text != null)
                ShowFramesPerSecond();
        }

        void ShowFramesPerSecond() {
            frames += 1;
            sec -= Time.deltaTime;
            if (sec <= 0) {
                text.text = frames + " fps";
                frames = 0;
                sec = 1f;
            }
        }
    }
}